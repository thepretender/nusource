//////////////////////////////////////////////////////////////////////////
// This file is a part of NotLimited.Framework.Common NuGet package.
// You are strongly discouraged from fiddling with it.
// If you do, all hell will break loose and living will envy the dead.
//////////////////////////////////////////////////////////////////////////
namespace NotLimited.Framework.Common.Helpers
{
	public class MinMaxTracker
	{
		public double Min = double.MaxValue;
		public double Max = double.MinValue;

		public void Track(params double[] numbers)
		{
			for (int i = 0; i < numbers.Length; i++)
			{
				if (numbers[i] > Max)
				{
					Max = numbers[i];
				}

				if (numbers[i] < Min)
				{
					Min = numbers[i];
				}
			}
		}
	}
}