//////////////////////////////////////////////////////////////////////////
// This file is a part of NotLimited.Framework.Common NuGet package.
// You are strongly discouraged from fiddling with it.
// If you do, all hell will break loose and living will envy the dead.
//////////////////////////////////////////////////////////////////////////
using System;
using System.Security.Cryptography;
using System.Threading;

namespace NotLimited.Framework.Common.Helpers
{
    public static class TrueRandom
    {
        private static readonly RNGCryptoServiceProvider _global = new RNGCryptoServiceProvider();

        private static readonly ThreadLocal<Random> _random = new ThreadLocal<Random>(() =>
        {
            byte[] buffer = new byte[4];
            _global.GetBytes(buffer);
            return new Random(BitConverter.ToInt32(buffer, 0));
        });

        public static Random Generator { get { return _random.Value; } }
    }
}