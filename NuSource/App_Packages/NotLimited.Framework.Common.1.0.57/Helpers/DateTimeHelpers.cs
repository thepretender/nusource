//////////////////////////////////////////////////////////////////////////
// This file is a part of NotLimited.Framework.Common NuGet package.
// You are strongly discouraged from fiddling with it.
// If you do, all hell will break loose and living will envy the dead.
//////////////////////////////////////////////////////////////////////////
using System;

namespace NotLimited.Framework.Common.Helpers
{
    public static class DateTimeHelpers
    {
        public static string ToShortDateTime(this DateTime time)
        {
            if (time.Date == DateTime.Now.Date)
            {
                return time.ToShortTimeString();
            }

            return time.ToShortDateString() + " " + time.ToShortTimeString();
        }

	    public static bool EqualsToSecond(this DateTime time, DateTime other)
	    {
		    return Math.Abs(time.Subtract(other).TotalSeconds) < 1;
	    }
    }
}