//////////////////////////////////////////////////////////////////////////
// This file is a part of NotLimited.Framework.Common NuGet package.
// You are strongly discouraged from fiddling with it.
// If you do, all hell will break loose and living will envy the dead.
//////////////////////////////////////////////////////////////////////////
using System;
using System.Threading;

namespace NotLimited.Framework.Common.Helpers
{
	public static class Awaiter
	{
		public static bool Await(Func<bool> func, TimeSpan timeout)
		{
			var startTime = DateTime.Now;
			while (true)
			{
				if (func())
				{
					return true;
				}

				if ((DateTime.Now - startTime) >= timeout)
				{
					return false;
				}

				Thread.Sleep(50);
			}
		}
	}
}