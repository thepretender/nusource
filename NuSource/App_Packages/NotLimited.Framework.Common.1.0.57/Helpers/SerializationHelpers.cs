//////////////////////////////////////////////////////////////////////////
// This file is a part of NotLimited.Framework.Common NuGet package.
// You are strongly discouraged from fiddling with it.
// If you do, all hell will break loose and living will envy the dead.
//////////////////////////////////////////////////////////////////////////
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace NotLimited.Framework.Common.Helpers
{
	public static class SerializationHelpers
	{
		public static void Serialize<T>(this T obj, Stream stream)
		{
			var serializer = new BinaryFormatter();
			serializer.Serialize(stream, obj);
		}

		public static T Deserialize<T>(this Stream stream)
		{
			var serializer = new BinaryFormatter();
			return (T)serializer.Deserialize(stream);
		}
	}
}