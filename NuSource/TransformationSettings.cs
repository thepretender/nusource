﻿namespace NuSource
{
    public class TransformationSettings
    {
        public TransformationSettings(bool makeInternal, bool useTargetNs, bool addWarning, string projectName)
        {
            MakeInternal = makeInternal;
            UseTargetNs = useTargetNs;
            AddWarning = addWarning;
            ProjectName = projectName;
        }

        public bool MakeInternal { get; private set; }
        public bool UseTargetNs { get; private set; }
        public bool AddWarning { get; private set; }
        public string ProjectName { get; private set; }
    }
}