﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using NotLimited.Framework.Common.Helpers;

namespace NuSource
{
	[Serializable]
	[XmlRoot("NuSource")]
	public class Config
	{
		[XmlAttribute]
		public string ProjectDir { get; set; }

		[XmlAttribute]
		public string OutDir { get; set; }

		[XmlAttribute]
		public string PackageName { get; set; }

		[XmlAttribute]
		public bool MakeInternal { get; set; }

		[XmlAttribute]
		public bool UseTargetNamespace { get; set; }

		[XmlAttribute]
		public bool AddWarning { get; set; }

		[XmlAttribute]
		public bool UseAppPackage { get; set; }

		public List<Filter> AlwaysPlaceInRoot { get; set; }

		public List<Dependency> Dependencies { get; set; }

		public List<Namespace> ReplaceNamespaces { get; set; }

		[XmlIgnore]
		public string NuspecPath { get; set; }

		[XmlIgnore]
		public string ProjectName { get; set; }

		[XmlIgnore]
		public string PackageSourceDir { get; set; }

		[XmlIgnore]
		public string NugetPath { get; set; }

		[XmlIgnore]
		public string NupkgPath { get; set; }

		public static Config Read(string path)
		{
			using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
			{
				var result = (Config)new XmlSerializer(typeof(Config)).Deserialize(stream);

				string configDir = Path.GetDirectoryName(path);

				// Compute full project dir
				if (result.ProjectDir.IsNullOrEmpty())
				{
					throw new InvalidOperationException("Project path is not specified!");
				}

				if (!Path.IsPathRooted(result.ProjectDir))
				{
					result.ProjectDir = new DirectoryInfo(Path.Combine(configDir, result.ProjectDir)).FullName;
				}

				// Compute out dir
				if (result.OutDir.IsNullOrEmpty())
				{
					result.OutDir = configDir;
				}
				else
				{
					result.OutDir = Path.IsPathRooted(result.OutDir) ? result.OutDir : new DirectoryInfo(Path.Combine(configDir, result.OutDir)).FullName;
				}

				result.ProjectName = new DirectoryInfo(result.ProjectDir).Name;
				result.PackageName = result.PackageName.IsNullOrEmpty() ? result.ProjectName : result.PackageName;
				result.PackageSourceDir = Path.Combine(result.OutDir, "sources");
				result.NuspecPath = Path.Combine(result.OutDir, result.PackageName + ".nuspec");
				result.NupkgPath = Path.Combine(result.OutDir, result.PackageName);
				result.NugetPath = FindNugetPath(configDir);

				return result;
			}
		}

		private static string FindNugetPath(string source)
		{
			var dir = new DirectoryInfo(source);
			string result;

			do
			{
				result = Path.Combine(dir.FullName, ".nuget", "nuget.exe");
				if (File.Exists(result))
				{
					return result;
				}
			} while ((dir = dir.Parent) != null);

			throw new InvalidOperationException("Failed to find NuGet binary!");
		}
	}

	[Serializable]
	public class Filter
	{
		[XmlIgnore]
		private Regex _compiledPathRegex;

		public Regex CompiledPathRegex { get { return _compiledPathRegex ?? (_compiledPathRegex = new Regex(PathRegex)); }}

		[XmlAttribute]
		public string PathRegex { get; set; }
	}

	[Serializable]
	public class Dependency
	{
		[XmlAttribute]
		public string Name { get; set; }

		[XmlAttribute]
		public string Version { get; set; }
	}

	[Serializable]
	public class Namespace
	{
		[XmlAttribute]
		public string Name { get; set; }
	}
}