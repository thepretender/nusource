﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CLAP;

namespace NuSource
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Parser.Run<NuSourceApp>(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("Done.");

#if DEBUG
            Console.ReadKey();
#endif
        }
    }
}
