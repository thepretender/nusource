﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using CLAP;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using NotLimited.Framework.Common.Helpers;
using NotLimited.Framework.Common.Helpers.Xml;


namespace NuSource
{
    public class NuSourceApp
    {
	    private const string Warning =
            @"//////////////////////////////////////////////////////////////////////////
// This file is a part of {0} NuGet package.
// You are strongly discouraged from fiddling with it.
// If you do, all hell will break loose and living will envy the dead.
//////////////////////////////////////////////////////////////////////////";


        private static readonly HashSet<string> _excludeDirs = new HashSet<string>(new[] { "obj", "bin", "Properties" }, StringComparer.OrdinalIgnoreCase);
        private static readonly HashSet<string> _excludeExtensions = new HashSet<string>(new [] {".csproj", ".tfssection", ".user", ".snk"}, StringComparer.OrdinalIgnoreCase);
		private static readonly HashSet<string> _excludeFiles = new HashSet<string>(new[] { "app.config", "packages.config" }, StringComparer.OrdinalIgnoreCase);
		private static readonly Dictionary<string, Func<string, TransformationSettings, Config, string>> _transformers = 
            new Dictionary<string, Func<string, TransformationSettings, Config, string>>
            {
                {".cs", TransformCs},
                {".xaml", TransformXaml}
            };
        
        private static readonly Dictionary<SyntaxKind, Func<BaseTypeDeclarationSyntax, SyntaxTokenList>> _modifierAccessors =
            new Dictionary<SyntaxKind, Func<BaseTypeDeclarationSyntax, SyntaxTokenList>>
            {
                {SyntaxKind.ClassDeclaration, syntax => ((ClassDeclarationSyntax)syntax).Modifiers},
                {SyntaxKind.InterfaceDeclaration, syntax => ((InterfaceDeclarationSyntax)syntax).Modifiers},
                {SyntaxKind.StructDeclaration, syntax => ((StructDeclarationSyntax)syntax).Modifiers},
                {SyntaxKind.EnumDeclaration, syntax => ((EnumDeclarationSyntax)syntax).Modifiers}
            };

        [Verb]
        public static void Publish([Required] string path)
   //         [DefaultValue("NuGet"), Aliases("o")]   string @out,
   //         [DefaultValue(true), Aliases("i")]      bool makeInternal,
   //         [DefaultValue(true), Aliases("n")]      bool useTargetNs,
   //         [DefaultValue(true), Aliases("w")]      bool addWarning,
   //         [DefaultValue(true), Aliases("a")]      bool useAppPackage,
   //         [Aliases("f")]                          string packageName)
        {
	        var config = Config.Read(path);

	        ProcessPackage(config);

            Console.WriteLine("Cleaning previous packages");
            var files = Directory.GetFiles(config.OutDir, config.PackageName + "*.nupkg");
            foreach (var file in files)
            {
                File.Delete(file);
            }

            Console.WriteLine("Starting Nuget to create the package");
            var process = Process.Start(new ProcessStartInfo(config.NugetPath, "pack \"" + config.NuspecPath + "\" -OutputDirectory \"" + config.OutDir + "\"")
                          {
                              CreateNoWindow = true,
                              RedirectStandardError = true,
                              RedirectStandardOutput = true,
                              UseShellExecute = false,
                          });
            string output = process.StandardOutput.ReadToEnd() + process.StandardError.ReadToEnd();
            process.WaitForExit();
            Console.Write(output);
            
            Console.WriteLine("Pushing packages");
            files = Directory.GetFiles(config.OutDir, config.PackageName + "*.nupkg");
            foreach (var file in files)
            {
                process = Process.Start(new ProcessStartInfo(config.NugetPath, "push \"" + file + "\" -Source nuget.org")
                              {
                                  CreateNoWindow = true,
                                  RedirectStandardError = true,
                                  RedirectStandardOutput = true,
                                  UseShellExecute = false
                              });
                output = process.StandardOutput.ReadToEnd() + process.StandardError.ReadToEnd();
                process.WaitForExit();
                Console.Write(output);
            }
        }

	    [Verb(IsDefault = true)]
        public static void ProcessPackage([Required] string path)
		{
			var config = Config.Read(path);
			ProcessPackage(config);
		}

        private static void ProcessPackage(Config config)
        {
            Console.WriteLine("Processing package " + config.PackageName);
            Console.WriteLine("Cleaning directories");
            if (!Directory.Exists(config.OutDir))
            {
                Directory.CreateDirectory(config.OutDir);
            }

            if (Directory.Exists(config.PackageSourceDir))
            {
                PathHelpers.DeleteDirectory(config.PackageSourceDir);
            }

            Directory.CreateDirectory(config.PackageSourceDir);

            Console.WriteLine("Copying project sources");
            var dstFiles = CopySources(config);

            Console.WriteLine("Transforming sources");
            var transformationSettings = new TransformationSettings(config.MakeInternal, config.UseTargetNamespace, config.AddWarning, config.ProjectName);
            dstFiles = TransformSources(dstFiles, transformationSettings, config);

            Console.WriteLine("Updating Nuspec");
            UpdateNuspec(config, dstFiles);
        }

        private static void UpdateNuspec(Config config, List<string> dstFiles)
        {
            XDocument doc;
            if (File.Exists(config.NuspecPath))
            {
                try
                {
                    doc = XDocument.Parse(File.ReadAllText(config.NuspecPath));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    doc = XDocument.Parse(Properties.Resources.Nuspec);
                }
            }
            else
                doc = XDocument.Parse(Properties.Resources.Nuspec);

            var metadata = doc.Root.ChildElement("metadata");
            string version = IncreaseVersion(metadata);
            SetId(metadata, config.PackageName);

            var files = doc.Root.ChildElement("files");
            if (files == null)
            {
                files = new XElement("files");
                doc.Root.AddElement(files);
            }
            else
            {
                foreach (var child in files.ChildElements("file").Where(x => x.AttributeValue("src").StartsWithOrdinal("sources\\")).ToList())
                {
                    child.Remove();
                }
            }

            foreach (var file in dstFiles)
            {
                string dstPath;
                if (config.UseAppPackage && !config.AlwaysPlaceInRoot.EmptyIfNull().Any(x => x.CompiledPathRegex.IsMatch(file)))
                {
                    dstPath = string.Format(@"content\App_Packages\{0}.{1}\{2}", config.ProjectName, version, PathHelpers.MakeRelative(file, config.PackageSourceDir));
                }
                else
                {
                    dstPath = string.Format(@"content\{0}", PathHelpers.MakeRelative(file, config.PackageSourceDir));
                }

                files.AddElement(new XElement("file",
                                              new XAttribute("src", PathHelpers.MakeRelative(file, config.OutDir)),
                                              new XAttribute("target", dstPath)));
            }

	        var dependencies = GetPackageDependencies(config);
	        if (config.Dependencies.Count > 0)
	        {
		        dependencies.AddRange(config.Dependencies.Where(x => !dependencies.ContainsKey(x.Name)).Select(x => new KeyValuePair<string, string>(x.Name, x.Version)));
	        }

	        if (dependencies.Count > 0)
	        {
		        var depsNode = metadata.ChildElement("dependencies");
		        if (depsNode == null)
		        {
			        depsNode = new XElement("dependencies");
					metadata.AddElement(depsNode);
		        }
		        else
		        {
			        depsNode.RemoveNodes();
		        }

		        foreach (var dependency in dependencies)
		        {
			        depsNode.AddElement(new XElement("dependency", new XAttribute("id", dependency.Key), new XAttribute("version", dependency.Value)));
		        }
	        }

            File.WriteAllText(config.NuspecPath, doc.ToString());
        }

	    private static Dictionary<string, string> GetPackageDependencies(Config config)
	    {
			var result = new Dictionary<string, string>();

		    string configPath = Path.Combine(config.ProjectDir, "packages.config");
		    if (!File.Exists(configPath))
		    {
			    return result;
		    }

		    XDocument doc;
		    try
		    {
				doc = XDocument.Load(configPath);
		    }
		    catch (Exception e)
		    {
			    Console.WriteLine("Error reading project's packages.config: " + e.ToString());
			    return result;
		    }

			result.AddRange(doc.Root.ChildElements("package").Select(x => new KeyValuePair<string, string>(x.AttributeValue("id"), x.AttributeValue("version"))));

		    return result;
	    }

        private static bool CanPlaceInAppPackage(string file)
        {
            var exploded = PathHelpers.ExplodePath(file);
            if (exploded.Length < 2)
            {
                return true;
            }

            if (!exploded[exploded.Length - 1].EqualsIgnoreCase("generic.xaml.pp"))
            {
                return true;
            }

            if (!exploded[exploded.Length - 2].EqualsIgnoreCase("Themes"))
            {
                return true;
            }

            return false;
        }

        private static List<string> TransformSources(List<string> dstFiles, TransformationSettings settings, Config config)
        {
            var result = new List<string>(dstFiles.Count);

            foreach (var file in dstFiles)
            {
                string filePath = file;
                string ext = Path.GetExtension(file).ToLowerInvariant();

                if (_transformers.ContainsKey(ext))
                {
                    string transformed = _transformers[ext](File.ReadAllText(file), settings, config);
                    filePath = file + ".pp";

                    File.Delete(file);
                    File.WriteAllText(filePath, transformed);
                }

                result.Add(filePath);
            }

            return result;
        }

        private static string TransformXaml(string input, TransformationSettings settings, Config config)
        {
            var nsRegex = new Regex(@"(clr-namespace:)" + settings.ProjectName);
            var classRegex = new Regex(@"(x:Class="")" + settings.ProjectName);

            string content = input;
            if (settings.UseTargetNs)
            {
                content = nsRegex.Replace(content, match => match.Groups[1].Value + "$rootnamespace$");
                content = classRegex.Replace(content, match => match.Groups[1].Value + "$rootnamespace$");
            }

            if (settings.AddWarning)
            {
                content = "<!--" + string.Format(Warning, settings.ProjectName) + "-->" + Environment.NewLine + content;
            }

            return content;
        }

        private static string TransformCs(string input, TransformationSettings settings, Config config)
        {
            var nsRegex = new Regex(@"(\s*namespace\s+)" + settings.ProjectName);
            var usingRegex = new Regex(@"(\s*using\s+)" + settings.ProjectName);

            string content = input;
            if (settings.MakeInternal)
            {
                content = ChangeVisibility(content);
            }

            if (settings.UseTargetNs)
            {
                content = nsRegex.Replace(content, match => match.Groups[1].Value + "$rootnamespace$");
                content = usingRegex.Replace(content, match => match.Groups[1].Value + "$rootnamespace$");

	            var additionalNs = config.ReplaceNamespaces.Select(x => new Regex(@"(\s*using\s+)" + x.Name));
	            foreach (var regex in additionalNs)
	            {
					content = regex.Replace(content, match => match.Groups[1].Value + "$rootnamespace$");
				}
            }

            if (settings.AddWarning)
            {
                content = string.Format(Warning, settings.ProjectName) + Environment.NewLine + content;
            }

            return content;
        }

	    private static List<string> CopySources(Config config)
        {
			var projectDir = new DirectoryInfo(config.ProjectDir);
            var sources = projectDir.EnumerateFiles()
                               .Where(x => !_excludeExtensions.Contains(x.Extension) && !_excludeFiles.Contains(x.Name))
                               .Concat(projectDir.GetDirectories()
                                            .Where(x => !_excludeDirs.Contains(x.Name))
                                            .SelectMany(dir => dir.EnumerateFiles("*", SearchOption.AllDirectories)
                                                                  .Where(x => !_excludeExtensions.Contains(x.Extension) && !_excludeFiles.Contains(x.Name))))
                               .ToList();

            var dstFiles = new List<string>();
            foreach (var file in sources)
            {
                string dstPath = PathHelpers.MakeAbsolute(PathHelpers.MakeRelative(file.FullName, projectDir.FullName), config.PackageSourceDir);
                string dstDir = Path.GetDirectoryName(dstPath);
                if (!Directory.Exists(dstDir))
                    Directory.CreateDirectory(dstDir);

                file.CopyTo(dstPath);
                dstFiles.Add(dstPath);
            }
            return dstFiles;
        }

        private static void SetId(XElement element, string id)
        {
            var node = element.ChildElement("id");
            if (node == null)
            {
                element.AddElement(new XElement("id", id));
            }
            else
            {
                if (string.IsNullOrEmpty(node.Value))
                    node.Value = id;
            }
        }

        private static string IncreaseVersion(XElement element)
        {
            var node = element.ChildElement("version");
            if (node == null)
            {
                node = new XElement("version", "1.0.0");
                element.AddElement(node);
            }
            else
            {
                if (string.IsNullOrEmpty(node.Value))
                {
                    node.Value = "1.0.0";
                }
                else
                {
                    var parts = node.Value.Split(new[] {'-'}, StringSplitOptions.RemoveEmptyEntries);
                    string verValue = parts.Length == 0 ? node.Value : parts[0];
                    var ver = Version.Parse(verValue);
                    verValue = string.Format("{0}.{1}.{2}", ver.Major, ver.Minor, ver.Build + 1);

                    node.Value = parts.Length == 0 ? verValue : parts.Skip(1).Aggregate(verValue, (s, s1) => s + "-" + s1);
                }
            }

            return node.Value;
        }

        private static string ChangeVisibility(string source)
        {
            var tree = SyntaxFactory.ParseSyntaxTree(source);
            var root = (CompilationUnitSyntax)tree.GetRoot();

            var decls = root.DescendantNodes()
                            .Where(x => x.IsKind(SyntaxKind.ClassDeclaration)
                                        || x.IsKind(SyntaxKind.InterfaceDeclaration)
                                        || x.IsKind(SyntaxKind.StructDeclaration)
                                        || x.IsKind(SyntaxKind.EnumDeclaration))
                            .Cast<BaseTypeDeclarationSyntax>()
                            .ToList();

            root = root.ReplaceNodes(decls, (syntax, rewrittenSyntax) =>
            {
                var modifiers = _modifierAccessors[rewrittenSyntax.Kind()](rewrittenSyntax);

                if (!modifiers.Any(x => x.Kind() == SyntaxKind.PublicKeyword))
                    return rewrittenSyntax;

                var token = modifiers.First(x => x.Kind() == SyntaxKind.PublicKeyword);

                return rewrittenSyntax.ReplaceToken(token, SyntaxFactory.Token(token.LeadingTrivia, SyntaxKind.InternalKeyword, token.TrailingTrivia));
            });

            return root.ToString();
        }
	}
}